# CKA <img align="right" width="250" height="250" src="cka-logo.png">    

CKA Exam related stuff can be found here.                                 

## Exam Details
- Duration: 2 hours
- Number of Tasks: 17+-
- Attempts: 2
- Validity of Cert : 3 years
- OS distro used: Ubuntu 20.04
- K8S version used: 1.23

## Learning Sources:
- https://kodekloud.com/courses/certified-kubernetes-administrator-cka/
- https://acloudguru.com/course/certified-kubernetes-administrator-cka

## Exam Tips 
- the official [Docu](https://kubernetes.io/docs/home/) is allowed
- use web browser's bookmarks for individual topics 
- you can use [Vivaldi](https://vivaldi.com/download/) web browser and its "Tile 2 Tabs" function
- name .yaml files with the number of Exam Task e.g. 1.yaml for better overview
   
## Useful Resources:
- https://milindchawre.github.io/site/blog/tips-to-ace-in-cka-and-ckad-exam/
- https://itnext.io/tips-tricks-for-cka-ckad-and-cks-exams-cc9dade1f76d
- https://docs.linuxfoundation.org/tc-docs/certification/tips-cka-and-ckad
- https://www.freecodecamp.org/news/certified-kubernetes-administrator-study-guide-cka/
- https://www.reddit.com/r/kubernetes/comments/ndwgkr/the_story_of_3_attempts_at_cka_and_how_killersh/
- https://killer.sh/
- https://kubewiz.com/
