###  Merge Kubernetes kubectl config files

##### Make a copy of your existing config 
``` 
cp ~/.kube/config ~/.kube/config.bak 
```

##### Merge the two config files together into a new config file 
```
$ KUBECONFIG=~/.kube/config:/path/to/new/config kubectl config view --flatten > /tmp/config 
```
##### Replace your old config with the new merged config 
```
$ mv /tmp/config ~/.kube/config 
```
##### (optional) Delete the backup once you confirm everything worked ok 
```
$ rm ~/.kube/config.bak
```

NOTE: user-names and cluster-names must differ

_More:_ https://medium.com/@jacobtomlinson/how-to-merge-kubernetes-kubectl-config-files-737b61bd517d


# Check Cert's validity
```
kubeadm certs check-expiration
```

# Display namespace based resources
```
kubectl api-resources --namespaced -o name
```

# Delete Pod brutforce
```
k delete pod --grace-period=0 --force
```
