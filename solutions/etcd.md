# ETCD Backup

```
master -> cat /etc/kubernetes/manifests/etcd.yaml |grep -e ca -e serv
    - --cert-file=/etc/kubernetes/pki/etcd/server.crt
    - --key-file=/etc/kubernetes/pki/etcd/server.key
    - --peer-trusted-ca-file=/etc/kubernetes/pki/etcd/ca.crt
    - --trusted-ca-file=**/etc/kubernetes/pki/etcd/ca.crt
  priorityClassName: system-node-critical

master -> export ETCDCTL_API=3

master -> etcdctl snapshot save /opt/etcd-backup-31.5.2022.db --cacert=/etc/kubernetes/pki/etcd/ca.crt --cert=/etc/kubernetes/pki/etcd/server.crt --key=/etc/kubernetes/pki/etcd/server.key
Snapshot saved at /opt/etcd-backup-31.5.2022.db
```

# ETCD Restore
```
master -> ls /opt/etcd-backup-31.5.2022.db
/opt/etcd-backup-31.5.2022.db

master -> mv /etc/kubernetes/manifests/etcd.yaml /var/tmp/

master -> mv /var/lib/etcd /var/lib/etcd-old

master -> etcdctl snapshot restore /opt/etcd-backup-31.5.2022.db --data-dir=/var/lib/etcd
2022-05-31 14:33:35.030114 I | mvcc: restore compact to 25095
2022-05-31 14:33:35.037225 I | etcdserver/membership: added member 8e9e05c52164694d [http://localhost:2380] to cluster cdf818194e3a8c32

master -> mv /var/tmp/etcd.yaml /etc/kubernetes/manifests/
```
